# Roadmap and Hoppr Objectives

This is the objective roadmap for Hoppr development.  As with any open source project, dates are subject to change and the roadmap will be updated accordingly.  


## Roadmap Gantt

```mermaid
gantt
dateFormat  YYYY-MM-DD
title Hoppr Timeline Goals

section Development
Begin Hoppr Development            : milestone, m1, 2022-01-01, 0m
Develop Hoppr                      :crit, 2022-01-01, 2022-10-01
Develop Bundling Capability         :active, 2022-02-01, 2022-07-01
Develop Nexus Bundling Plugin       :active, 2022-06-01, 2022-09-01
Integration Testing                 :      , 2022-07-01, 2022-09-30
Release Hoppr MVP                   : milestone, m2, 2022-09-30, 0m
```

## Roadmap Item Details

### Develop Bundling Capability

__Acceptance Criteria__

* Manifest, Transfer, and Credentials files can be consumed and processed
* Manifest and Transfer file is validated against a schema 
* All SBOMs are consumed and Validated
* Validate collected artifacts against SBOM (compare hashes, checksums, or other pattern of verification)
* Collect all SBOMs and manifest and package them as part of the transfer for hoppr bundle metadata and traceability
* Build Container Image of Hoppr for CI Runtimes
* Complete Flat File Transfer Plugin


### Develop Nexus Bundling Plugin

__Acceptance Criteria__

This should be the first _third party plugin_. 

* All components in flat file structure are collected into a Nexus bundle
* Nexus bundle can be spun up and artifacts exist in the appropriate location


### Integration Testing

__Acceptance Criteria__

* For each plugin (to date) / and the core:  Automate the verification process and prove that the tool works as intended
* Test environment (initially a network-isolated job)
* Test without isolation
* Test with isolation (i.e. airgap scenario)

__Context:__

* This feature will encapsulate the integration, testing, and verification of `hoppr`.  The solution should provide proof that the solution works: Collecting Assets, Packaging Assets, and a mechanism to bundle (both flat-file and Nexus).
* This process should be automated.
* This should spin up a target environment that is completely isolated from external components.
* This should provide the base test platform for which plugin creators can expand to test their plugins.
* Testing environments will evolve as different component types drive testing needs
