/**
 * custom.js
 * A wrapper for termynal.js to load markdown.
 *
 * @author Sebastián Ramírez
 * @url https://github.com/tiangolo/typer/blob/a1520dcda685220a9a796288f5eaaebd00d68845/docs/js/custom.js 
 * @version a1520dcda685220a9a796288f5eaaebd00d68845
 * @license MIT
 */

document.querySelectorAll(".use-termynal").forEach(node => {
  node.style.display = "block";
  new Termynal(node, {
      lineDelay: 500
  });
});
const progressLiteralStart = "---> 100%";
const promptLiteralStart = "$ ";
const customPromptLiteralStart = "# ";
const termynalActivateClass = "termy";
let termynals = [];

function createTermynals() {
  document
      .querySelectorAll(`.${termynalActivateClass} .highlight`)
      .forEach(node => {
          const text = node.textContent;
          const lines = text.split("\n");
          const useLines = [];
          let buffer = [];
          function saveBuffer() {
              if (buffer.length) {
                  let isBlankSpace = true;
                  buffer.forEach(line => {
                      if (line) {
                          isBlankSpace = false;
                      }
                  });
                  dataValue = {};
                  if (isBlankSpace) {
                      dataValue["delay"] = 0;
                  }
                  if (buffer[buffer.length - 1] === "") {
                      // A last single <br> won't have effect
                      // so put an additional one
                      buffer.push("");
                  }
                  const bufferValue = buffer.join("<br>");
                  dataValue["value"] = bufferValue;
                  useLines.push(dataValue);
                  buffer = [];
              }
          }
          for (let line of lines) {
              if (line === progressLiteralStart) {
                  saveBuffer();
                  useLines.push({
                      type: "progress"
                  });
              } else if (line.startsWith(promptLiteralStart)) {
                  saveBuffer();
                  const value = line.replace(promptLiteralStart, "").trimEnd();
                  useLines.push({
                      type: "input",
                      value: value
                  });
              } else if (line.startsWith("// ")) {
                  saveBuffer();
                  const value = "💬 " + line.replace("// ", "").trimEnd();
                  useLines.push({
                      value: value,
                      class: "termynal-comment",
                      delay: 0
                  });
              } else if (line.startsWith(customPromptLiteralStart)) {
                  saveBuffer();
                  const promptStart = line.indexOf(promptLiteralStart);
                  if (promptStart === -1) {
                      console.error("Custom prompt found but no end delimiter", line)
                  }
                  const prompt = line.slice(0, promptStart).replace(customPromptLiteralStart, "")
                  let value = line.slice(promptStart + promptLiteralStart.length);
                  useLines.push({
                      type: "input",
                      value: value,
                      prompt: prompt
                  });
              } else {
                  buffer.push(line);
              }
          }
          saveBuffer();
          const div = document.createElement("div");
          node.replaceWith(div);
          const termynal = new Termynal(div, {
              lineData: useLines,
              noInit: true,
              lineDelay: 500
          });
          termynals.push(termynal);
      });
}

function loadVisibleTermynals() {
  termynals = termynals.filter(termynal => {
      if (termynal.container.getBoundingClientRect().top - innerHeight <= 0) {
          termynal.init();
          return false;
      }
      return true;
  });
}
window.addEventListener("scroll", loadVisibleTermynals);
createTermynals();
loadVisibleTermynals();

// Remove extra footer on home view
if (window.location.pathname === "/" || window.location.pathname === "/index.html") {
    if (document.querySelectorAll('.md-footer').length === 2) {
        document.querySelectorAll('.md-footer')[1].remove();
    }
    document.querySelector(".md-main").style.position = "absolute";
}

const chromeAgent = window.navigator.userAgent.indexOf("Chrome") !== -1;
const safariAgent = window.navigator.userAgent.indexOf("Safari") !== -1;
const modzillaAgent = window.navigator.userAgent.indexOf("Firefox") !== -1;

// If safari
if (!chromeAgent && safariAgent) {
    document.querySelectorAll(".mdx-parallax__group")[0].style.contain = "none";
}

const mdxHero = document.querySelector(".mdx-hero");
const mdxParallax = document.querySelector(".mdx-parallax");
const mdxTabs = document.querySelector(".md-tabs__list");

mdxParallax.addEventListener('scroll', () => {
    // If chrome
    if (chromeAgent) {
        if (mdxParallax.scrollTop >= 1660 && !mdxTabs.hasAttribute("hidden")) {
            mdxTabs.setAttribute("hidden", "");
        } else if (mdxParallax.scrollTop < 1660 && mdxTabs.hasAttribute("hidden")) {
            mdxTabs.removeAttribute("hidden");
        }
    }
    // if firefox
    if (!chromeAgent && modzillaAgent) {
        if (mdxParallax.scrollTop >= 5 && !mdxHero.hasAttribute("hidden")) {
            mdxHero.setAttribute("hidden", "");
            mdxTabs.setAttribute("hidden", "");
        } else if (mdxParallax.scrollTop < 5 && mdxHero.hasAttribute("hidden")) {
            mdxHero.removeAttribute("hidden");
            mdxTabs.removeAttribute("hidden");
        }
    }
})