
# Overview

__Background__

Hoppr is your software supply-chain (SBOM) utility kit.  Using a simple plugin architecture, Hoppr can collect, process,
and bundle your digital assets to streamline transfers from one production environment to another - especially useful for 
airgap scenarios.

With _Data being the new Oil_, the entire industry has seen that there's a rapidly-expanding collection of new possibilities when using the Hoppr tool.

A few of the many possibilities:

  1. Collect, bundle, and deploy artifacts as defined by a manifest and collection of BOMs.
  2. Release products to be consumed by other teams through a manifest.
  3. Extend to provide further information on the contents of the transfer.

**Goals**:

- ```Package```  :: Framework to collect digital assets and build dependencies for consolidated packaging
- ```Verify```   :: Secure Software Supply Chain Management of these dependencies
- ```Transfer``` :: Abstract the transfer method across environment boundaries
- ```Delivery``` :: Consolidated packages delivered to target location

**Key Features**:

- Standardized workflow
- Infinitely extendable with plugins
- Batteries-included for core plugins (collect, process, bundle)

## Install
Install [Hoppr from PyPI](https://pypi.org/project/hoppr/){target=_blank}.

<div class="termy">

```console
$ pip install hoppr

---> 100%

hopctl --help
```

</div>

## Contributing
Learn more about contributing with our [contribution recommendations](contributing.md).

## License
Hoppr is [MIT Licensed](https://gitlab.com/lmco/hoppr/hoppr/-/raw/main/LICENSE){target=_blank}.
