# Usage

## Prerequisites

The following prerequisites are required to use Hoppr:

1. `Python 3.10` or higher
2. Executables (e.g. `mvn`, `skopeo`, etc) required by selected Hoppr plugins for SBOM processing
3. Manifest, Transfer, and Credentials files.  See [Inputs](/getting_started/inputs.html)

## Install Hoppr to use the hopctl command line tool

You can install Hoppr simply by running the following command:

<div class="termy">

```console
$ pip install hoppr

$ hopctl version
Hoppr Framework Version: {{ hoppr.version }}
Python Version: 3.10.4
```

</div>

## Creating a bundle

You can create a simple `tar.gz` bundle with the following command.  Assuming that you have the following files already defined.

1. [Manifest](/getting_started/inputs#manifests)
2. [Credentials](/getting_started/inputs#credentials)
3. [Transfer](/getting_started/inputs#transfers)

<div class="termy">

```console
$ hopctl bundle manifest.yml --credentials credentials.yml --transfer transfer.yml
```

</div>

<!--

::: Commenting out until content is ready

## Customizing
TBD

## Questions
TBD

-->
