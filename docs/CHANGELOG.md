## [0.0.4](https://gitlab.com/lmco/hoppr/hoppr-docs/compare/v0.0.3...v0.0.4) (2022-11-07)


### Bug Fixes

* Correct links in usage docs ([ff9a43f](https://gitlab.com/lmco/hoppr/hoppr-docs/commit/ff9a43f9917b790501f849dd62d96bc003f242aa))
* Reference hoppr-docs site for edit links ([377b16e](https://gitlab.com/lmco/hoppr/hoppr-docs/commit/377b16e9c6031748e2e5bd953845fc7325bb2c7c))

## [0.0.3](https://gitlab.com/lmco/hoppr/hoppr-docs/compare/v0.0.2...v0.0.3) (2022-11-03)


### Bug Fixes

* Correct preview url ([f2c49c8](https://gitlab.com/lmco/hoppr/hoppr-docs/commit/f2c49c81ca210d60bd39dac958f30173e4b268d0))

## [0.0.2](https://gitlab.com/lmco/hoppr/hoppr-www/compare/v0.0.1...v0.0.2) (2022-11-03)


### Bug Fixes

* Add git sign off ([074e27d](https://gitlab.com/lmco/hoppr/hoppr-www/commit/074e27d6fae74380777ba1890cf58298f0e1b81b))
* Add renovate ([105684e](https://gitlab.com/lmco/hoppr/hoppr-www/commit/105684e3a1780e16d999c19ac8b403af96de8641))
* add workflow ([1fb351f](https://gitlab.com/lmco/hoppr/hoppr-www/commit/1fb351fa8e8f539aa9e7f83e966e0879d0467159))
* Clean up stages ([ceb7dd6](https://gitlab.com/lmco/hoppr/hoppr-www/commit/ceb7dd688836967e5c7a0141a0d8a999ed638fdf))
* Need changelog stub ([a12f53c](https://gitlab.com/lmco/hoppr/hoppr-www/commit/a12f53c715937a2e28749552404c9101fc657dcb))
* Remove old changelog ([b2605cf](https://gitlab.com/lmco/hoppr/hoppr-www/commit/b2605cf45eb6a2590b78a9eda7a22adacb5fc073))
* Set poetry lock file ([8186123](https://gitlab.com/lmco/hoppr/hoppr-www/commit/81861233a8144b2b29fc9f6008b41392c56d0e68))

# Changelog
