# Sequence Diagram for Hoppr

## Pre Processing Sequence Diagram

Gather various aspects need to perform processing.

```mermaid
sequenceDiagram
    participant U as User
    participant H as hopctl
    participant sv as Schema Validation
    participant co as Create Object
    participant vp as Verify Plugin
    U->>+H: Run hopctl
    Note right of H: Provide manifest file
    Note right of H: Provide transfer file
    Note right of H: Provide credentials file
    H->>+sv: Validate Transfer File
    sv->>+co: Create Transfer Object
    co-->>-H: Return Transfer Object
    loop foreach plugin in Transfer Object
        H-->>+vp: Initialize and validate functionality
    end
    H->>+sv: Validate Credentials File
    sv->>+co: Create Credentials Object
    co-->>-H: Return Credentials Object
    H->>+sv: Validate Manifest File
    sv->>+co: Create Manifest Object
    loop for each include recursively gather manifests
        co-->>-H: Return Manifest Object
    end
    Note right of H: Creates Manifest Object Array
    loop for each sbom in Manifest Object Array
        H->>+co: Get SBOM contents
        co-->>-H: Return CycloneDX Sbom Object
    end
    Note right of H: Creates SBOM Object Array
```

## Collect Components Sequence Diagram

```mermaid
sequenceDiagram
    participant dd as Dedupe
    participant down as Download and Validate
    participant walk as Walk Directories
    H->>+dd: Combine(?) and dedupe Components in SBOM Object Array
    dd-->>-H: Return deduped SBOM Object for processing
    loop for each component in SBOM Object
        loop for each plugin in Transfer Object
            H->>+down: Pass credentials object and component to collector plugin
            down-->>-H: Return Result Object
        end
    end
    H->>+walk: Get directory structure
    Note right of walk: Return :: Flat file directory structure
    Note right of walk: Return :: Combined SBOM(?)
    Note right of walk: Return :: STDOUT Log
    Note right of walk: Return :: Error Log
    Note right of walk: Return :: Processing Metadata
```

### Complete Proof Of Concept Workflow

```mermaid
sequenceDiagram
    participant U as User
    participant H as hopctl
    participant sv as Schema Validation
    participant co as Create Object
    participant vp as Verify Plugin
    participant dd as Dedupe
    participant down as Download and Validate
    participant walk as Walk Directories

    U->>+H: Run hopctl
    Note right of H: Provide manifest file
    Note right of H: Provide transfer file
    Note right of H: Provide credentials file
    H->>+sv: Validate Transfer File
    sv->>+co: Create Transfer Object
    co-->>-H: Return Transfer Object
    loop foreach plugin in Transfer Object
        H-->>+vp: Initialize and validate functionality
    end
    H->>+sv: Validate Credentials File
    sv->>+co: Create Credentials Object
    co-->>-H: Return Credentials Object
    H->>+sv: Validate Manifest File
    sv->>+co: Create Manifest Object
    loop for each include recursively gather manifests
        co-->>-H: Return Manifest Object
    end
    Note right of H: Creates Manifest Object Array
    loop for each sbom in Manifest Object Array
        H->>+co: Get SBOM contents
        co-->>-H: Return CycloneDX Sbom Object
    end
    Note right of H: Creates SBOM Object Array
    H->>+dd: Combine(?) and dedupe Components in SBOM Object Array
    dd-->>-H: Return deduped SBOM Object for processing
    Note right of dd: Not sure if this makes sense
    loop for each component in SBOM Object
        loop for each plugin in Transfer Object
            H->>+down: Pass credentials object and component to collector plugin
            down-->>-H: Return Result Object
        end
    end
    H->>+walk: Get directory structure
    Note right of walk: Return :: Flat file directory structure
    Note right of walk: Return :: Combined SBOM(?)
    Note right of walk: Return :: STDOUT Log
    Note right of walk: Return :: Error Log
    Note right of walk: Return :: Processing Metadata
```
