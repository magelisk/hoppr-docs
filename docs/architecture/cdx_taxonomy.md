## `hoppr` Namespace Taxonomy

Context: [What is CycloneDX Property Taxonomy?](https://github.com/CycloneDX/cyclonedx-property-taxonomy)

| Namespace | Description | Administered By | Taxonomy |
| --- | --- | --- | --- |
| `hoppr:repository` | Namespace for properties specific to Hoppr repositories | Lockheed Martin Hoppr | [hoppr:repository taxonomy](#hoppr:repository) |
| `hoppr:schema` | Namespace for properties specific to Hoppr input file schemas | Lockheed Martin Hoppr | [hoppr:schema taxonomy](#hoppr:schema) |

## `hoppr:repository` Namespace Taxonomy

| Namespace             | Description                                                       |
| --------------------- | ----------------------------------------------------------------- |
| `hoppr:repository:component_search_sequence`  | The ordered list of respositories used to find a component |

## `hoppr:schema` Namespace Taxonomy

| Namespace             | Description                                                       |
| --------------------- | ----------------------------------------------------------------- |
| `hoppr:schema:manifest_version`  | Manifest file schema version |
| `hoppr:schema:transfer_version`  | Transfer file schema version |
| `hoppr:schema:credentials_version`  | Credentials file schema version |
